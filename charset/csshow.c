/*
 * csshow.c: display a character-set table on a terminal.
 *
 * Can display a selected block of Unicode, or the whole of any
 * single-byte character set for which libcharset knows the
 * translation.
 *
 * Intended mostly for quick-reference use - it might very well be
 * quicker to type 'csshow U+0400' than to click around for ages in a
 * browser finding the appropriate Unicode chart. But it also works
 * well as a test of the specific font you've configured in your
 * terminal window, of course.
 *
 * Possible extra features:
 *  - configurable row len and table size.
 *  - option to disambiguate the various classes of failure in the
 *    output, e.g. if terminfo gives us control sequences to change
 *    colours then we could colour the missing characters differently
 *    depending on why they're missing.
 *     + this mode probably implies that we must also display all
 *       characters in the range, whether printable or not, because
 *       the whole point might be to disambiguate the various causes
 *       of undisplayability. (In particular, don't forget to turn off
 *       the early exit when nothing in the range is printable at
 *       all.)
 *  - ability to display sub-blocks of multibyte encodings such as
 *    EUCs. But that would need some thought about how to sensibly
 *    index those tables.
 */

#define _XOPEN_SOURCE 500              /* for wcwidth and snprintf */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <ctype.h>

#ifndef HAVE_NO_WCWIDTH
#include <wchar.h>
#endif

#ifndef HAVE_NO_WCTYPE
#include <wctype.h>
#endif

#include "charset.h"

static const char *helptext =
    "usage: csshow ( CHARSET | BASE-UNICODE-VALUE )\n"
    " e.g.: csshow Win1252\n"
    "       csshow U+2500\n"
    " also: csshow --help          display this help text\n"
    ;

static void help(FILE *fp)
{
    fputs(helptext, fp);
}

enum Trans {
    BAD_CHAR_IN_SOURCE_CHARSET,
    BAD_CHAR_IN_OUTPUT_CHARSET,
    UNPRINTABLE_CHAR,
    FIRST_PRINTABLE_VALUE,
    COMBINING_CHAR = FIRST_PRINTABLE_VALUE,
    WIDE_PRINTABLE_CHAR,
    NORMAL_PRINTABLE_CHAR
};
struct translated_char {
    enum Trans type;
    char buf[7]; /* maximum even theoretical UTF-8 code length, plus NUL */
};

int main(int argc, char **argv)
{
    int doing_opts = 1;
    int source_charset = CS_ASCII, output_charset = CS_NONE;
    unsigned long base = 0, size = 0x100, rowlen = 0x10;

    while (--argc > 0) {
        const char *p = *++argv;
        if (*p == '-' && doing_opts) {
            if (!strcmp(p, "--")) {
                doing_opts = 0;
            } else if (!strcmp(p, "--help")) {
                help(stdout);
                return 0;
            } else {
                fprintf(stderr, "csshow: unrecognised option '%s'\n", p);
                return 1;
            }
        } else {
            int cs;

            if (toupper((unsigned char)p[0]) == 'U' &&
                (p[1] == '-' || p[1] == '+')) {
                source_charset = CS_NONE; /* means just translate Unicode */
                base = strtoul(p+2, NULL, 16);
            } else if ((cs = charset_from_localenc(p)) != CS_NONE) {
                if (!charset_is_single_byte(cs)) {
                    fprintf(stderr, "csshow: cannot display multibyte"
                            " charset %s\n", charset_to_localenc(cs));
                    return 1;
                }
                source_charset = cs;
                base = 0;
            } else {
                fprintf(stderr, "csshow: unrecognised argument '%s'\n", p);
                return 1;
            }
        }
    }

#ifndef HAVE_NO_WCTYPE
    setlocale(LC_CTYPE, "");
#endif

    if (output_charset == CS_NONE)
        output_charset = charset_from_locale();

    {
        struct translated_char *trans;
        const char *rowheadfmt;
        int rowheadwidth, colwidth;
        int printed_a_line, skipped_a_line;
        unsigned long i, j;

        trans = malloc(size * sizeof(struct translated_char));
        if (!trans) {
            fprintf(stderr, "csshow: out of memory\n");
            return 1;
        }

        /*
         * Initial loop figuring out what characters we have in our
         * block, and in what way each of them is weird.
         */
        for (i = 0; i < size; i++) {
            unsigned long charcode = base + i;
            wchar_t wc;

            trans[i].buf[0] = '\0';

            if (source_charset == CS_NONE) {
                wc = charcode;
            } else {
                char c = charcode;
                const char *cp = &c;
                int clen = 1;
                int error = 0;

                int ret = charset_to_unicode(
                    &cp, &clen, &wc, 1, source_charset, NULL, L"", 0);
                if (ret != 1) {
                    trans[i].type = BAD_CHAR_IN_SOURCE_CHARSET;
                    continue;
                }
            }

            {
                const wchar_t *wcp = &wc;
                int wclen = 1;
                int error = 0;

                int ret = charset_from_unicode(
                    &wcp, &wclen, trans[i].buf, sizeof(trans[i].buf) - 1,
                    output_charset, NULL, &error);

                assert(ret < sizeof(trans[i].buf));
                trans[i].buf[ret] = '\0';

                if (wclen != 0 || ret == 0 || error) {
                    trans[i].type = BAD_CHAR_IN_OUTPUT_CHARSET;
                    trans[i].buf[0] = '\0';
                    continue;
                }
            }

            /*
             * OK, we have a Unicode character and a corresponding
             * UTF-8 sequence. But it might still be something we have
             * to take care over printing.
             */
#ifndef HAVE_NO_WCTYPE
            if (!iswprint(wc)) {
                trans[i].type = UNPRINTABLE_CHAR;
                trans[i].buf[0] = '\0';
                continue;
            }
#endif
            {
#ifndef HAVE_NO_WCWIDTH
                int width = wcwidth(wc);
#else
                int width = 1;
#endif

                switch (width) {
                  case 0:
                    trans[i].type = COMBINING_CHAR;
                    break;
                  case 1:
                    trans[i].type = NORMAL_PRINTABLE_CHAR;
                    break;
                  case 2:
                    trans[i].type = WIDE_PRINTABLE_CHAR;
                    break;
                  default:
                    /* If we somehow had wcwidth but not wctype, weird
                     * returns from wcwidth give us a second way to
                     * identify non-printable control characters. */
                    trans[i].type = UNPRINTABLE_CHAR;
                    trans[i].buf[0] = '\0';
                    break;
                }
            }
        }

        /*
         * Special case: if _nothing_ in our range is printable, we'll
         * just terminate now.
         */
        for (i = 0; i < size; i++)
            if (trans[i].type >= FIRST_PRINTABLE_VALUE)
                break;
        if (i == size) {
            fprintf(stderr, "csshow: nothing printable at all in this"
                    " character range\n");
            return 1;
        }

        /*
         * Now we can figure out whether there are any wide
         * characters, in which case we should space out our table a
         * bit more. (We might also have to do that if rowlen is
         * large.)
         */
        {
            char testbuf[64];
            colwidth = snprintf(testbuf, sizeof(testbuf),
                                "%-2x", (unsigned)(rowlen-1));
        }
        if (colwidth < 3) {
            for (i = 0; i < size; i++)
                if (trans[i].type == WIDE_PRINTABLE_CHAR)
                    colwidth = 3;
        }

        /*
         * Work out the width of the heading column on the left.
         */
        if (source_charset == CS_NONE) {
            rowheadfmt = "U+%-6.4X";
        } else {
            rowheadfmt = "%-4.2X";
        }
        {
            char testbuf[64];
            rowheadwidth = snprintf(testbuf, sizeof(testbuf),
                                    rowheadfmt, (unsigned)(base + (size-1)));
        }

        /* Heading line. */
        printf("%*s", rowheadwidth, "");
        for (i = 0; i < rowlen; i++)
            printf("%-*X", colwidth, (unsigned)i);
        printf("\n");

        printed_a_line = skipped_a_line = 0;

        for (j = 0; j < size; j += rowlen) {
            /* See if we're skipping this row completely. */
            int skip = 1;
            for (i = 0; i < rowlen && j+i < size; i++)
                if (trans[j+i].type >= FIRST_PRINTABLE_VALUE)
                    skip = 0;
            if (skip) {
                skipped_a_line = 1;
                continue;
            }

            /* We're printing this line, but we might need to print a
             * blank line to indicate a previous skipped one. But we
             * don't do that at the very start or end - we only
             * indicate a skipped line between two that _were_
             * printed. */
            if (skipped_a_line && printed_a_line) {
                printf("\n");
            }
            skipped_a_line = 0;

            printed_a_line = 1;
            printf(rowheadfmt, (unsigned)(base + j));;
            for (i = 0; i < rowlen && j+i < size; i++) {
                int chars_left = colwidth;
                struct translated_char *chr = &trans[j+i];

                switch (chr->type) {
                  case COMBINING_CHAR:
                    /* Print a space first, for the combining char to
                     * safely combine with. */
                    printf(" %s", chr->buf);
                    chars_left--;
                    break;
                  case WIDE_PRINTABLE_CHAR:
                    fputs(chr->buf, stdout);
                    chars_left -= 2;
                    break;
                  case NORMAL_PRINTABLE_CHAR:
                    fputs(chr->buf, stdout);
                    chars_left--;
                    break;
                  default:
                    /* Unprintable for one reason or another. */
                    break;
                }

                if (i+1 < rowlen && j+i+1 < size)
                    printf("%*s", chars_left, "");
            }
            printf("\n");
        }
    }

    return 0;
}
